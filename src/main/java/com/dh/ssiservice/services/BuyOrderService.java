package com.dh.ssiservice.services;

import com.dh.ssiservice.model.BuyOrder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BuyOrderService extends GenericService<BuyOrder> {
    List<BuyOrder> findByUnit(String unit);
}
