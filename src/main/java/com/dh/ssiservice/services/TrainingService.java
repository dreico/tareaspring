package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Training;

import java.util.List;


public interface TrainingService extends GenericService<Training> {
    List<Training> findBySkill(String skill);
}
