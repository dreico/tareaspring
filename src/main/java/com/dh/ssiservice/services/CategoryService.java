/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Category;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService extends GenericService<Category> {
    List<Category> findByCode(String code);
}
