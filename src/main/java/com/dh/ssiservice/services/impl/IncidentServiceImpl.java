package com.dh.ssiservice.services.impl;

import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.repositories.IncidentRepository;
import com.dh.ssiservice.services.IncidentService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IncidentServiceImpl extends GenericServiceImpl<Incident> implements IncidentService {
    private IncidentRepository incidentRepository;

    public IncidentServiceImpl(IncidentRepository incidentRepository) {
        this.incidentRepository = incidentRepository;
    }


    @Override
    protected CrudRepository<Incident, Long> getRepository() {
        return incidentRepository;
    }

    @Override
    public List<Incident> findByCode(String code) {
        List<Incident> incidents = new ArrayList<>();
        incidentRepository.findByCode(code).get().iterator().forEachRemaining(incidents::add);
        return incidents;
    }
}
