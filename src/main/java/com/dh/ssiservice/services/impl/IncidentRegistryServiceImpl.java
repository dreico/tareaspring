/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services.impl;

import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.repositories.IncidentRegistryRepository;
import com.dh.ssiservice.services.IncidentRegistryService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class IncidentRegistryServiceImpl extends GenericServiceImpl<IncidentRegistry> implements IncidentRegistryService {
    private IncidentRegistryRepository incidentRegistryRepository;

    public IncidentRegistryServiceImpl(IncidentRegistryRepository incidentRegistryRepository) {
        this.incidentRegistryRepository = incidentRegistryRepository;
    }

    public List<IncidentRegistry> findByCause(String cause) {
        List<IncidentRegistry> incidentRegistries = new ArrayList<>();
        incidentRegistryRepository.findByCause(cause).get().iterator().forEachRemaining(incidentRegistries::add);
        return incidentRegistries;
    }

    @Override
    protected CrudRepository<IncidentRegistry, Long> getRepository() {
        return incidentRegistryRepository;
    }
}
