/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services.impl;

import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.repositories.TrainingRepository;
import com.dh.ssiservice.services.TrainingService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TrainingServiceImpl extends GenericServiceImpl<Training> implements TrainingService {
    private TrainingRepository trainingRepository;

    public TrainingServiceImpl(TrainingRepository trainingRepository) {
        this.trainingRepository = trainingRepository;
    }

    public List<Training> findBySkill(String skill) {
        List<Training> trainings = new ArrayList<>();
        trainingRepository.findBySkill(skill).get().iterator().forEachRemaining(trainings::add);
        return trainings;
    }

    @Override
    protected CrudRepository<Training, Long> getRepository() {
        return trainingRepository;
    }
}
