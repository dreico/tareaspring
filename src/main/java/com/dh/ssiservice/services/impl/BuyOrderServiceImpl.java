/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services.impl;

import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.repositories.BuyOrderRepository;
import com.dh.ssiservice.services.BuyOrderService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BuyOrderServiceImpl extends GenericServiceImpl<BuyOrder> implements BuyOrderService {
    private BuyOrderRepository buyOrderRepository;

    public BuyOrderServiceImpl(BuyOrderRepository buyOrderRepository) {
        this.buyOrderRepository = buyOrderRepository;
    }

    public List<BuyOrder> findByUnit(String unit) {
        List<BuyOrder> trainings = new ArrayList<>();
        buyOrderRepository.findByUnit(unit).get().iterator().forEachRemaining(trainings::add);
        return trainings;
    }

    @Override
    protected CrudRepository<BuyOrder, Long> getRepository() {
        return buyOrderRepository;
    }
}
