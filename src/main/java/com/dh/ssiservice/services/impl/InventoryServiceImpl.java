/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services.impl;

import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.repositories.InventoryRepository;
import com.dh.ssiservice.services.InventoryService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class InventoryServiceImpl extends GenericServiceImpl<Inventory> implements InventoryService {
    private InventoryRepository inventoryRepository;

    public InventoryServiceImpl(InventoryRepository inventoryRepository) {
        this.inventoryRepository = inventoryRepository;
    }

    public List<Inventory> findByWarehouse(String warehouse) {
        List<Inventory> trainings = new ArrayList<>();
        inventoryRepository.findByWarehouse(warehouse).get().iterator().forEachRemaining(trainings::add);
        return trainings;
    }

    @Override
    protected CrudRepository<Inventory, Long> getRepository() {
        return inventoryRepository;
    }
}
