package com.dh.ssiservice.services;

import com.dh.ssiservice.model.IncidentRegistry;
import org.springframework.stereotype.Service;

import java.util.List;


public interface IncidentRegistryService extends GenericService<IncidentRegistry> {
    List<IncidentRegistry> findByCause(String cause);
}
