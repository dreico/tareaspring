package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incident;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IncidentService extends GenericService<Incident> {
    List<Incident> findByCode(String code);
}
