package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.IncidentRegistry;

import java.util.Date;

public class IncidentRegistryCommand {
    private Long id;
    private Date date;
    private String cause;
    private String infraction;
    private Double cuantific;

    private Long incidentId;
    private String codeIncident;
    private String nameIncident;
    private Long areaId;
    private String codeArea;
    private String nameArea;

    public IncidentRegistryCommand(IncidentRegistry incidentRegistry) {
        this.id = incidentRegistry.getId();
        this.date = incidentRegistry.getDate();
        this.cause = incidentRegistry.getCause();
        this.infraction = incidentRegistry.getInfraction();
        this.cuantific = incidentRegistry.getCuantific();
        this.incidentId = incidentRegistry.getIncident().getId();
        this.codeIncident = incidentRegistry.getIncident().getCode();
        this.nameIncident = incidentRegistry.getIncident().getName();
        this.areaId = incidentRegistry.getArea().getId();
        this.codeArea = incidentRegistry.getArea().getCode();
        this.nameArea = incidentRegistry.getArea().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getInfraction() {
        return infraction;
    }

    public void setInfraction(String infraction) {
        this.infraction = infraction;
    }

    public Double getCuantific() {
        return cuantific;
    }

    public void setCuantific(Double cuantific) {
        this.cuantific = cuantific;
    }

    public Long getIncidentId() {
        return incidentId;
    }

    public void setIncidentId(Long incidentId) {
        this.incidentId = incidentId;
    }

    public String getCodeIncident() {
        return codeIncident;
    }

    public void setCodeIncident(String codeIncident) {
        this.codeIncident = codeIncident;
    }

    public String getNameIncident() {
        return nameIncident;
    }

    public void setNameIncident(String nameIncident) {
        this.nameIncident = nameIncident;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public String getCodeArea() {
        return codeArea;
    }

    public void setCodeArea(String codeArea) {
        this.codeArea = codeArea;
    }

    public String getNameArea() {
        return nameArea;
    }

    public void setNameArea(String nameArea) {
        this.nameArea = nameArea;
    }
    public IncidentRegistry toDomain() {
        IncidentRegistry incidentRegistry = new IncidentRegistry();
        incidentRegistry.setId(getId());
        incidentRegistry.setDate(getDate());
        incidentRegistry.setCause(getCause());
        incidentRegistry.setInfraction(getInfraction());
        incidentRegistry.setCuantific(getCuantific());
        return incidentRegistry;
    }
}
