package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.Position;
import com.dh.ssiservice.model.Training;

import java.util.Date;

public class TrainingCommand {
    private Long id;
    private String skill;
    private Date date;
    private String areaCode;
    private String areaName;
    private String position;

    public TrainingCommand(Training training) {
        this.id = training.getId();
        this.skill = training.getSkill();
        this.date = training.getDate();
        this.areaCode = training.getArea().getCode();
        this.areaName = training.getArea().getName();
        this.position = training.getPosition().getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Training toDomain() {
        Area area = new Area();
        area.setCode(getAreaCode());
        area.setName(getAreaName());
        Position position = new Position();
        position.setName(getPosition());
        Training training = new Training();
        training.setSkill(getSkill());
        training.setDate(getDate());
        training.setArea(area);
        training.setPosition(position);
        return training;
    }
}
