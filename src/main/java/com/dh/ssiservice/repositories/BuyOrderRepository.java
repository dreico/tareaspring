package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.BuyOrder;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BuyOrderRepository extends CrudRepository<BuyOrder, Long> {
    Optional<List<BuyOrder>> findByUnit(String unit);
}
