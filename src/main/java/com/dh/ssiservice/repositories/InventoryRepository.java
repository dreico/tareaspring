package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Inventory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface InventoryRepository extends CrudRepository<Inventory, Long> {
    Optional<List<Inventory>> findByWarehouse(String warehouse);
}
