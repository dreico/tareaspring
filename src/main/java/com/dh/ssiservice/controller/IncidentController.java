package com.dh.ssiservice.controller;

import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.services.IncidentService;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/incidents")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class IncidentController {
    private IncidentService incidentService;

    public IncidentController(IncidentService incidentService) {
        this.incidentService = incidentService;
    }


    @GET
    public Response getIncidents() {
//        List<Incident> categories = categoryService.findByCode(code);
//        Response.ResponseBuilder responseBuilder = Response.ok(categories);
//        addCorsHeader(responseBuilder);
        List<Incident> incidents = incidentService.findAll();
        Response.ResponseBuilder responseBuilder = Response.ok(incidents);
        addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    @OPTIONS
    public Response prefligth() {
        return Response.ok().build();
    }

    private void addCorsHeader(Response.ResponseBuilder responseBuilder) {
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }
}
